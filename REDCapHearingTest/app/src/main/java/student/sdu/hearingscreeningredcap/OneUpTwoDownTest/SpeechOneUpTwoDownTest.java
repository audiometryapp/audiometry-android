package student.sdu.hearingscreeningredcap.OneUpTwoDownTest;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;

import java.util.Map;

import student.sdu.hearingscreeningredcap.activities.SpeechAudiometryActivity;
import student.sdu.hearingscreeningredcap.activities.Test1Activity;
import student.sdu.hearingscreeningredcap.application.HearingScreeningApplication;
import student.sdu.hearingscreeningredcap.databasemanagement.TestDAO;
import student.sdu.hearingscreeningredcap.translators.StartDBTranslator;

public class SpeechOneUpTwoDownTest extends Activity {

    SpeechOneUpTwoDownTest speechtest = new SpeechOneUpTwoDownTest();
    int ear; // instantiated here, gets value later.
    MediaPlayer mp = new MediaPlayer();
    boolean testActive = false;
    Map<Integer, Float> phoneMaxDBOutput;
    StartDBTranslator startDBTranslator = new StartDBTranslator();
    int testFreqNo = 0;
    float dbhl = startDBTranslator.translate(testFreqNo);

    public SpeechOneUpTwoDownTest() {
        TestDAO testDAO = new TestDAO(HearingScreeningApplication.getContext());
        phoneMaxDBOutput = testDAO.getCalibrationValues(); // CALIBRATION
        System.out.println("Speech1U2D class instantiated successfully!");
    }

    public void submitInput (String input){
        System.out.println(input);
    }

        private int getEar () {
            return ear;
        }

    public float getAmplitude()
    {
        float amplitude = (float) Math.pow(10,(dbhl-phoneMaxDBOutput.get(testFreqNo))/20);
        return amplitude;
    }

    public void playSound ()
        {
            HearingScreeningApplication.setMusicStreamVolumeMax(); //Enforce the calibration expected by the test.
            // error when trying to create mediaplayer - SpeechAudiometryTest not fully encapsulated
             mp = MediaPlayer.create(SpeechOneUpTwoDownTest.this, speechtest.getSoundFile());
            float amplitude = speechtest.getAmplitude();
            if (speechtest.getEar() == 0) {
                mp.setVolume(amplitude, 0f);
            } else {
                mp.setVolume(0f, amplitude);
            }
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                /*Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 500);*/
                    mp.release();
                }
            });
            mp.start();
        }

        private int getSoundFile () {
            System.out.println("Sound file received.");
            return 0;
        }

        private void OneUpTwoDown () {

        }


        public boolean isTestActive () {
            return testActive;
        }
    }