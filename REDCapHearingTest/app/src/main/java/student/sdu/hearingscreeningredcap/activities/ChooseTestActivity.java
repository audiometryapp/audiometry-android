package student.sdu.hearingscreeningredcap.activities;

import android.content.Intent;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.InetAddress;

import student.sdu.hearingscreeningredcap.R;
import student.sdu.hearingscreeningredcap.application.HearingScreeningApplication;

public class ChooseTestActivity extends AppCompatActivity
{

    private String recordID;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_test);


        Button btn = (Button) findViewById(R.id.btn_BaselineTest);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                choseActivity("baseline_arm_1");
            }
        });


        btn = (Button) findViewById(R.id.btn_VisitOneTest);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                choseActivity("visit_1_arm_1");
            }
        });

        btn = (Button) findViewById(R.id.btn_skip_to_speechtest);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                skipToSpeechTest();
            }
        });
    }

    private void choseActivity(String redCapTestEvent)
    {
            Intent i = new Intent(this, Test1Activity.class);
            i.putExtra("recordID", recordID);
            startActivity(i);
            this.finish();
    }

    private void skipToSpeechTest() {
            Intent i = new Intent(this, SpeechAudiometryActivity.class);
            startActivity(i);
            this.finish();
    }


    @Override
    public void onBackPressed()
    {
        HearingScreeningApplication.activityIntentSwitch(new MainMenuActivity(), this);
    }
}
