package student.sdu.hearingscreeningredcap.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import student.sdu.hearingscreeningredcap.R;
import student.sdu.hearingscreeningredcap.application.HearingScreeningApplication;


public class SettingsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        Button btn = (Button) findViewById(R.id.btn_calibrate);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                HearingScreeningApplication.activityIntentSwitch(new CalibrateActivity(), SettingsActivity.this);
            }
        });

        btn = (Button) findViewById(R.id.btn_calibrationData);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                HearingScreeningApplication.activityIntentSwitch(new CalibrationDataActivity(), SettingsActivity.this);
            }
        });


        btn = (Button) findViewById(R.id.btn_volumetest);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HearingScreeningApplication.activityIntentSwitch(new VolumeTestActivity(), SettingsActivity.this);
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        HearingScreeningApplication.activityIntentSwitch(new MainMenuActivity(), this);

    }
}
