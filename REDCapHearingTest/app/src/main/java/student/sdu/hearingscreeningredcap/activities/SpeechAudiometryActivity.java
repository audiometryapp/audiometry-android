package student.sdu.hearingscreeningredcap.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import student.sdu.hearingscreeningredcap.OneUpTwoDownTest.SpeechOneUpTwoDownTest;
import student.sdu.hearingscreeningredcap.R;
import student.sdu.hearingscreeningredcap.application.HearingScreeningApplication;

public class SpeechAudiometryActivity extends AppCompatActivity {

//   private SpeechOneUpTwoDownTest speechTest;
   private EditText input;
   private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speech_audiometry);
        // speechTest = new SpeechOneUpTwoDownTest();
        input = (EditText) findViewById(R.id.edittxt_user_input);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerInput();
            }
        });
    }

    @Override
    public void onBackPressed() {
        HearingScreeningApplication.activityIntentSwitch(new MainMenuActivity(), SpeechAudiometryActivity.this);
    }


    private void registerInput() {
        String userInput = input.getText().toString();
    //    if (userInput != "" && speechTest.isTestActive()) {
    //        speechTest.submitInput(userInput);
    //    }
        System.out.println(userInput);

    }

}
