package student.sdu.hearingscreeningredcap.activities;

import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import student.sdu.hearingscreeningredcap.R;
import student.sdu.hearingscreeningredcap.application.HearingScreeningApplication;

public class SplashScreenActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setSplashScreenTime(3);
    }

    /**
     * sets the display time of the splashscreen to time given.
     * Input should be in seconds.
     * @param time
     */
    private void setSplashScreenTime (int time)
    {
        new Handler().postDelayed(new Runnable()
        {
            public void run()
            {
                HearingScreeningApplication.activityIntentSwitch(new MainMenuActivity(), SplashScreenActivity.this);
            }
        }, (time * 1000));
    }
}
