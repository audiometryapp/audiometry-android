package student.sdu.hearingscreeningredcap.activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.SystemClock;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import student.sdu.hearingscreeningredcap.OneUpTwoDownTest.OneUpTwoDownTest;
import student.sdu.hearingscreeningredcap.R;
import student.sdu.hearingscreeningredcap.application.HearingScreeningApplication;
import student.sdu.hearingscreeningredcap.dataclasses.TestDTO;

public class Test1Activity extends AppCompatActivity
{
    private TextView topInfoTv;
    private TextView bottomInfoTv;
    private TextView testStateInfoText;
    private Button yesBtn;
    private Button noBtn;
    private ProgressBar testProgressBar;
    private OneUpTwoDownTest test;
    private boolean testOver = false;
    private MediaPlayer mp;
    private long testStartTime;
    private Button btnHeardSound;
    public boolean isSoundPlayed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test1);
        topInfoTv = (TextView) findViewById(R.id.tv_test_info_top);
        bottomInfoTv = (TextView) findViewById(R.id.tv_test_info_bot);
        testStateInfoText = (TextView) findViewById(R.id.tv_test_state);
        yesBtn = (Button)findViewById(R.id.btn_yes);
        noBtn = (Button)findViewById(R.id.btn_no);
        testProgressBar = (ProgressBar)findViewById(R.id.pb_test1_activity);
        testStartTime = SystemClock.elapsedRealtime();
        btnHeardSound = (Button) findViewById(R.id.btn_heard_sound);

        btnHeardSound.setVisibility(View.GONE);


        Intent intent = getIntent();

        test = new OneUpTwoDownTest(new TestDTO(intent.getExtras().getString("recordID")));

        bottomInfoTv.setVisibility(View.INVISIBLE);
        yesBtn.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (test.isCalibrationEmpty())
                    {
                        Toast.makeText(HearingScreeningApplication.getContext(),
                                "Der er ikke foretaget kalibrering med dette device\n" +
                                        "Venligst gør dette i samarbejde med klinikken",
                                Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        test();
                    }
                }
            });
        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HearingScreeningApplication.activityIntentSwitch(new MainMenuActivity(), Test1Activity.this);
            }
        });
        HearingScreeningApplication.setMusicStreamVolumeMax();
    }

    /**
     * Starts the testing process, and ends it when the process is complete.
     */
    private void test()
    {
        btnHeardSound.setVisibility(View.VISIBLE);
        btnHeardSound.setEnabled(true);

        yesBtn.setEnabled(false);
        yesBtn.setVisibility(View.GONE);
        noBtn.setEnabled(false);
        noBtn.setVisibility(View.GONE);
        if(testOver)
        {

            String testSavedCondition;
            if (test.isSaved())
            {
                testSavedCondition = "Resultatet er gemt korrekt.";
            }
            else
            {
                testSavedCondition = "Men resultatet kunne ikke gemmes.";
            }

            topInfoTv.setText("Testen er nu færdiggjort " + testSavedCondition);
            bottomInfoTv.setText("Tryk på Resultat for at komme til resultat skærmen\nEller Menu" +
                    "for at vende til menuen");

           /* yesBtn.setVisibility(View.GONE);
            yesBtn.setClickable(false);*/

            btnHeardSound.setText("Menu");
            btnHeardSound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mainIntent = new Intent(getApplicationContext(), MainMenuActivity.class);
                    Test1Activity.this.startActivity(mainIntent);
                    Test1Activity.this.finish();
                }
            });

            //yesBtn.setEnabled(true);
            //noBtn.setEnabled(true);
        }
        else
        {
            topInfoTv.setText("Testen er i gang.");
            String ear = ( test.getEar() == 0) ? "Venstre øre" : "Højre øre";
            testStateInfoText.setText("Lyt med: " + ear + "\n" +
                                        "Tone: " + (test.getTestFreqNo()+1) + "/8");
            bottomInfoTv.setVisibility(View.INVISIBLE);
            /*noBtn.setVisibility(View.GONE);
            yesBtn.setVisibility(View.GONE);
            yesBtn.setEnabled(false);
            noBtn.setEnabled(false);*/


            final Runnable timeout = new Runnable() {
                @Override
                public void run() {
                    testOver = test.answer(false, timeTaken());
                    test();
                }
            };
            final Handler timeoutHandler = new Handler();
            //Wait 1 second before playing the sound
            Handler handler = new Handler();
             handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // topInfoTv.setText("En tone afspilles!");
                    playSound();
                    isSoundPlayed = true;
                    if (isSoundPlayed) {
                        topInfoTv.setText("Testen er i gang.");
                        btnHeardSound.setEnabled(true);
                        timeoutHandler.postDelayed(timeout, 5000);
                    } else {
                        btnHeardSound.setEnabled(false);
                        topInfoTv.setText(R.string.test_tv_top_text_2);
                    }
                }
            }, getRandomizedWaitTime());





            btnHeardSound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    timeoutHandler.removeCallbacks(timeout);
                    testOver = test.answer(true, timeTaken());
                    updateProgressBar(test.getCSteps(), test.getMSteps());
                    test();
                }
            });

        }
    }
    private int getRandomizedWaitTime() {
        return (int)((Math.random() * 20) + 10);
    }

    private long timeTaken()
    {
        return (testStartTime - SystemClock.elapsedRealtime());
    }

    /**
     * Plays the sound used for testing.
     * When the sound is completed, the yes/no buttons are enabled.
     * The frequency is based on a class variable, used to keep the frequencies sequential in testing.
     * The volume is based on the wanted DB Hearing Level, and is found using the calculateAmplitude() method.
     */
    private void playSound()
    {
        HearingScreeningApplication.setMusicStreamVolumeMax(); //Enforce the calibration expected by the test.
        mp = MediaPlayer.create(Test1Activity.this, test.getSoundFile());
        float amplitude = test.getAmplitude();
        if(test.getEar() == 0) {
            mp.setVolume(amplitude, 0f);
        } else {
            mp.setVolume(0f, amplitude);
        }
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                /*Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 500);*/
                mp.release();
            }
        });
        mp.start();
    }

    @Override
    public void onBackPressed()
    {
        HearingScreeningApplication.activityIntentSwitch(new MainMenuActivity(), this);
    }

    private void updateProgressBar(float cSteps, float mSteps)
    {
        cSteps++;
        int progress = Math.round((cSteps/mSteps)*100f);
        testProgressBar.setProgress(progress);
    }
}
