package student.sdu.hearingscreeningredcap.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import student.sdu.hearingscreeningredcap.R;
import student.sdu.hearingscreeningredcap.application.HearingScreeningApplication;
import student.sdu.hearingscreeningredcap.application.PlayerConfig;

public class VideoExplanationActivity extends YouTubeBaseActivity
{
    private YouTubePlayerView youTubePlayerView;
    private Button btn;
    private YouTubePlayer.OnInitializedListener onInitializedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_explanation);

        btn = (Button) findViewById(R.id.btn_ve_startVid);
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.player_view);
        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b)
            {
                youTubePlayer.loadVideo("lebQe1FfbG4");
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult)
            {

            }
        };

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                youTubePlayerView.initialize(PlayerConfig.API_KEY, onInitializedListener);
            }
        });
    }

    @Override
    public void onBackPressed() {
        HearingScreeningApplication.activityIntentSwitch(new MainMenuActivity(), this);
    }
}
