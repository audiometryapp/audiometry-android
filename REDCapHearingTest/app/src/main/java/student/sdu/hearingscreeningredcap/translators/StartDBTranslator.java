package student.sdu.hearingscreeningredcap.translators;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ChrisPCBeast on 29-04-2018.
 */

public class StartDBTranslator
{
    private Map<Integer, Float> translateMap;

    public StartDBTranslator() {
        initializeMap();
    }

    public float translate(int frequencyNumber) {
        return translateMap.get(frequencyNumber);
    }

    private void initializeMap()
    {
        translateMap = new HashMap();
        translateMap.put(0, 58.0f);  // 38 SPL -> 20 HL      // 48 SPL -> 30 HL         // 58 SPL -> 40 HL
        translateMap.put(1, 51.0f);  // 31 SPL -> 20 HL      // 41 SPL -> 30 HL         // 51 SPL -> 40 HL
        translateMap.put(2, 45.5f);   // 25.5 SPL -> 20 HL    // 35.5 SPL -> 30 HL      // 45.5 SPL -> 40 HL
        translateMap.put(3, 44.5f);   // 24.5 SPL -> 20 HL    // 34.5 SPL -> 30 HL      // 44.5 SPL -> 40 HL
        translateMap.put(4, 42.5f);   // 22.5 SPL -> 20 HL    // 32.5 SPL -> 30 HL      // 42.5 SPL -> 40 HL
        translateMap.put(5, 49.5f);   // 29.5 SPL -> 20 HL    // 39.5 SPL -> 30 HL      // 49.5 SPL -> 40 HL
        translateMap.put(6, 57.0f);  // 37 SPL -> 20 HL      // 47 SPL -> 30 HL         // 57 SPL -> 40 HL
        translateMap.put(7, 57.5f);  // 37.5 SPL -> 20 HL    // 47.5 SPL -> 30 HL       // 57.5 SPL -> 40 HL
    }
}